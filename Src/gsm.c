#include "gsm.h"
#include "usart.h"
#include <string.h>
#include <stdarg.h>



void gsm_setup(struct gsm_module* gsm, UART_HandleTypeDef* huart,
              uint8_t* rx_buffer, uint8_t rx_buffer_size,
              uint8_t* tx_buffer, uint8_t tx_buffer_size,
              uint8_t* return_buffer, uint8_t return_buffer_size)
{
    gsm->huart              = huart;
    gsm->rx_buffer          = rx_buffer;
    gsm->rx_buffer_size     = rx_buffer_size;
    gsm->tx_buffer          = tx_buffer;
    gsm->tx_buffer_size     = tx_buffer_size;
    gsm->return_buffer      = return_buffer;
    gsm->return_buffer_size = return_buffer_size;
    gsm->return_data        = 0;
    gsm->rx_completed       = 1;
    gsm->rx_buffer_index    = 0;
    gsm->response_type      = GSM_OK;
}



void gsm_init(struct gsm_module* gsm)
{
    HAL_UART_Receive_IT(gsm->huart, (uint8_t *)&gsm->rx_byte, 1);

    gsm_sendAT(gsm, "AT+QIMODE=%i\r", 0);
    gsm_waitFor(gsm, GSM_OK, GSM_RESPONSE_TIMEOUT);

    gsm_sendAT(gsm, "AT+QINDI=%i\r", 0);
    gsm_waitFor(gsm, GSM_OK, GSM_RESPONSE_TIMEOUT);

    gsm_sendAT(gsm, "AT+QICSGP=%i,\"%s\",\"%s\",\"%s\"\r", 1, "wap.orange.md", "", "", 0);
    gsm_waitFor(gsm, GSM_OK, GSM_RESPONSE_TIMEOUT);
}

void gsm_getResponseType(struct gsm_module* gsm)
{
    if (strstr((char*)gsm->rx_buffer, GSM_RESPONSE_CMD) != NULL)
    {
        gsm->response_type = GSM_CMD;
    }
    else if (strstr((char*)gsm->rx_buffer, GSM_RESPONSE_OK) != NULL)
    {
        gsm->response_type = GSM_OK;
    }
    else if (strstr((char*)gsm->rx_buffer, GSM_RESPONSE_CONNECT) != NULL)
    {
        gsm->response_type = GSM_CONNECT;
    }
    else if (strstr((char*)gsm->rx_buffer, GSM_RESPONSE_ERROR) != NULL)
    {
        gsm->response_type = GSM_ERROR;
    }
    else
    {
        gsm->response_type = GSM_DATA;
        memcpy(gsm->return_buffer, gsm->rx_buffer, gsm->return_buffer_size);
        gsm->return_data = 0;
    }
}



void gsm_waitFor(struct gsm_module* gsm, enum gsm_response_type rsp_type, uint32_t timeout)
{
    uint32_t timer_start = HAL_GetTick();
    while((gsm->response_type != rsp_type) && (HAL_GetTick() - timer_start) <= timeout);
    gsm_getResponseType(gsm);
}



void gsm_sendAT(struct gsm_module* gsm, char* cmd, ...)
{
    va_list args;
    va_start(args, cmd);
    memset(gsm->tx_buffer, 0, gsm->tx_buffer_size);
    vsprintf((char*)(gsm->tx_buffer), cmd, args);
    HAL_UART_Transmit_IT(gsm->huart, gsm->tx_buffer, gsm->tx_buffer_size);
    va_end(args);
    HAL_Delay(200);
}



void gsm_httpSetUrl(struct gsm_module* gsm, char* url, uint32_t timeout)
{
    gsm_sendAT(gsm, "AT+QHTTPURL=%i,%i\r", strlen(url), timeout);
    gsm_waitFor(gsm, GSM_CONNECT, GSM_RESPONSE_TIMEOUT);

    gsm_sendAT(gsm, "%s\r\n", url);
    gsm_waitFor(gsm, GSM_OK, GSM_RESPONSE_TIMEOUT);
}



void gsm_httpPOST(struct gsm_module* gsm, char* data_to_send, uint32_t input_time, uint32_t timeout)
{
    gsm_sendAT(gsm, "AT+QHTTPPOST=%i,%i,%i\r", strlen(data_to_send), input_time, timeout);
    gsm_waitFor(gsm, GSM_CONNECT, GSM_RESPONSE_TIMEOUT);
    gsm_sendAT(gsm, data_to_send);
    gsm_waitFor(gsm, GSM_OK, GSM_RESPONSE_TIMEOUT);
}



void gsm_httpGET(struct gsm_module* gsm, uint32_t timeout)
{
    gsm_sendAT(gsm, "AT+QHTTPGET=%i\r", timeout);
    gsm_waitFor(gsm, GSM_OK, GSM_RESPONSE_TIMEOUT);
}



void gsm_httpRead(struct gsm_module* gsm, uint8_t save_response, uint32_t timeout)
{
    gsm_sendAT(gsm, "AT+QHTTPREAD=%i\r", timeout);
    gsm_waitFor(gsm, GSM_CONNECT, GSM_RESPONSE_TIMEOUT);
    gsm->return_data = 1;
    gsm_waitFor(gsm, GSM_OK, GSM_RESPONSE_TIMEOUT);
}



void gsm_openConnection(struct gsm_module* gsm, char* cmd, ...)
{
    va_list args;
    va_start(args, cmd);
    gsm_sendAT(gsm, cmd, args);
    va_end(args);
}