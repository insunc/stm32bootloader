#include "bootloader.h"
#include "gsm.h"


static uint32_t* flash_ptr        = APP_START_ADDR;
static uint32_t* reflash_flag_ptr = APP_START_ADDR;
static uint8_t index = 0;



void bootloader_init()
{
    __HAL_RCC_SYSCFG_CLK_ENABLE();
    __HAL_RCC_FLASH_CLK_ENABLE();

    HAL_FLASH_Unlock();
    __HAL_FLASH_CLEAR_FLAG(FLASH_FLAG_BSY | FLASH_FLAG_EOP | FLASH_FLAG_PGERR | FLASH_FLAG_WRPERR);
    HAL_FLASH_Lock();

    // find the reflash flag in reflash flag page
    // reflash_flag_ptr = APP_START_ADDR;
    // while ((*reflash_flag_ptr != 0x1) && (reflash_flag_ptr != FLASH_STOP_ADDR))
    // {
    //     reflash_flag_ptr++;
    // }

    // if (reflash_flag_ptr == FLASH_STOP_ADDR)
    // {
    //     // jump to app
    // }
}



uint8_t bootloader_eraseApp(void)
{
    uint32_t page_error = 0;
    FLASH_EraseInitTypeDef erase_conf;
    HAL_StatusTypeDef erase_status = HAL_OK;

    erase_conf.NbPages = (APP_STOP_ADDR - APP_START_ADDR) / FLASH_PAGE_SIZE;
    erase_conf.PageAddress = (uint32_t)APP_START_ADDR;
    erase_conf.TypeErase = FLASH_TYPEERASE_PAGES;

    HAL_FLASH_Unlock();

    erase_status = HAL_FLASHEx_Erase(&erase_conf, &page_error);

    HAL_FLASH_Lock();

    return (erase_status == HAL_OK) ? BOOTLOADER_OK : BOOTLOADER_ERASE_ERROR;
}



void bootloader_writeApp(struct gsm_module* gsm)
{
    uint8_t err_cnt = 0;
    flash_ptr = APP_START_ADDR;

    if (gsm->response_type == GSM_OK)
    {
        gsm_httpSetUrl(gsm, FIRMWARE_SERVER_URL, 30);
        if (gsm->response_type == GSM_OK)
        {
            gsm_httpPOST(gsm, "update", 20, 10);
            gsm_httpRead(gsm, 0, 20);
            if(gsm->response_type == GSM_OK)
            {
                HAL_FLASH_Unlock();
                while(strstr((char*)(gsm->return_buffer), STOP_REFLASH_RESPONSE) == NULL)
                {
                    gsm_httpGET(gsm, 20);
                    gsm_httpRead(gsm, 1, 20);
                    gsm_sendAT(gsm, gsm->return_buffer);
                    // for (index = 0; index < RX1_BUFFER_SIZE; index++)
                    // {
                    //     HAL_FLASH_Program(FLASH_TYPEPROGRAM_WORD, flash_ptr, gsm->return_buffer[index]);
                    // }
                }
                HAL_FLASH_Lock();
            }
        }
    }
}