#ifndef _GSM_H_
#define _GSM_H_

#include <stdint.h>
#include "usart.h"

/* Response strings */
#define GSM_RESPONSE_CMD     "AT+"
#define GSM_RESPONSE_OK      "OK"
#define GSM_RESPONSE_CONNECT "CONNECT"
#define GSM_RESPONSE_ERROR   "ERROR"

#define GSM_RESPONSE_TIMEOUT 3000

enum gsm_response_type
{
    GSM_CMD,
    GSM_OK,
    GSM_CONNECT,
    GSM_ERROR,
    GSM_DATA
};

struct gsm_module
{
    UART_HandleTypeDef* huart;

    uint8_t rx_byte;
    uint8_t return_data  : 1;
    uint8_t rx_completed : 1;
    uint8_t rx_buffer_index;

    uint8_t* rx_buffer;
    uint8_t* tx_buffer;
    uint8_t* return_buffer;

    uint8_t rx_buffer_size;
    uint8_t tx_buffer_size;
    uint8_t return_buffer_size;

    enum gsm_response_type response_type;

};

void gsm_setup(struct gsm_module* gsm, UART_HandleTypeDef* huart,
              uint8_t* rx_buffer, uint8_t rx_buffer_size,
              uint8_t* tx_buffer, uint8_t tx_buffer_size,
              uint8_t* return_buffer, uint8_t return_buffer_size);

void gsm_init(struct gsm_module* gsm);

void gsm_getResponseType(struct gsm_module* gsm);

void gsm_sendAT(struct gsm_module* gsm, char* cmd, ...);
void gsm_waitFor(struct gsm_module* gsm, enum gsm_response_type rsp_type, uint32_t timeout);

void gsm_httpGET(struct gsm_module* gsm, uint32_t timeout);
void gsm_httpPOST(struct gsm_module* gsm, char* data_to_send, uint32_t input_time, uint32_t timeout);
void gsm_httpRead(struct gsm_module* gsm, uint8_t save_response, uint32_t timeout);

#endif