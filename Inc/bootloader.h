#ifndef _BOOTLOADER_H_
#define _BOOTLOADER_H_

#include <stdint.h>
#include "usart.h"

#define FIRMWARE_SERVER_URL       "http://178.168.113.196:5000/"
#define START_REFLASH_RESPONSE    "connected"
#define STOP_REFLASH_RESPONSE     "done"

#define FLASH_START_ADDR          (uint32_t*)0x08000000
#define FLASH_STOP_ADDR           (uint32_t*)0x08010000
#define REFLASH_FLAG_START_ADDR   (uint32_t*)(FLASH_STOP_ADDR - FLASH_PAGE_SIZE)

#define APP_START_ADDR            (uint32_t*)0x08002CA0
#define APP_STOP_ADDR             (uint32_t*)REFLASH_FLAG_START_ADDR



enum bootloader_status
{
    BOOTLOADER_OK,
    BOOTLOADER_ERASE_ERROR,
    BOOTLOADER_WRITE_ERROR,
};



void bootloader_init();
uint8_t bootloader_eraseApp(void);
void bootloader_writeApp(struct gsm_module* gsm);
void bootloader_findFlagAddr(void);

#endif